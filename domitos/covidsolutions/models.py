from django.db import models

# Create your models here.
from django.db import models
from wagtail.core.models import Page
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.core import blocks

from wagtail.core.fields import StreamField

from streams import blocks

# Create your models here.

class CovidSolutions(Page):

    

    header_title = models.CharField(max_length=200, blank=True, null=True)
    meta_content = models.CharField(max_length=200, blank=True, null=True) 

    subtitle = models.CharField(max_length=100, null=True, blank= True)

    subtitle1 = models.CharField(max_length=100, null=True, blank= True)

    cardtitle1 = models.CharField(max_length=100, null=True, blank= True)

    cardtext1 = models.CharField(max_length=100, null=True, blank= True)

    cardtitle2 = models.CharField(max_length=100, null=True, blank= True)

    cardtext2 = models.CharField(max_length=100, null=True, blank= True)


    cardtitle3 = models.CharField(max_length=100, null=True, blank= True)

    cardtext3 = models.CharField(max_length=100, null=True, blank= True)


    cardtitle4 = models.CharField(max_length=100, null=True, blank= True)

    cardtext4 = models.CharField(max_length=100, null=True, blank= True)

    cardtitle5 = models.CharField(max_length=100, null=True, blank= True)

    cardtext5 = models.CharField(max_length=100, null=True, blank= True)

    cardtitle6 = models.CharField(max_length=100, null=True, blank= True)

    cardtext6 = models.CharField(max_length=100, null=True, blank= True)

    cardtitle7 = models.CharField(max_length=100, null=True, blank= True)

    cardtext7 = models.CharField(max_length=100, null=True, blank= True)

    cardtitle8 = models.CharField(max_length=100, null=True, blank= True)

    cardtext8 = models.CharField(max_length=100, null=True, blank= True)

    content1 = StreamField(
        [ 
            ("simple_richtext", blocks.SimpleRichtextBlock()),
        
        ],
        null=True,
        blank=True
    )
    content2 = StreamField(
        [ 
            ("simple_richtext", blocks.SimpleRichtextBlock()),
        ],
        null=True,
        blank=True

    )
    content3 = StreamField(
        [ 
            ("simple_richtext", blocks.SimpleRichtextBlock()),
        ],
        null=True,
        blank=True
    )

    content4 = StreamField(
        [ 
            ("simple_richtext", blocks.SimpleRichtextBlock()),
        ],
        null=True,
        blank=True
    )

    content5 = StreamField(
        [ 
            ("simple_richtext", blocks.SimpleRichtextBlock()),
        ],
        null=True,
        blank=True
    )

    question_title=models.CharField(max_length=500, null=True, blank= True)

    question1=models.CharField(max_length=500, null=True, blank= True)
    answer1=models.CharField(max_length=500, null=True, blank= True)

    question2=models.CharField(max_length=500, null=True, blank= True)
    answer2=models.CharField(max_length=500, null=True, blank= True)


    question3=models.CharField(max_length=500, null=True, blank= True)
    answer3=models.CharField(max_length=500, null=True, blank= True)


    question4=models.CharField(max_length=500, null=True, blank= True)
    answer4=models.CharField(max_length=500, null=True, blank= True)


    question5=models.CharField(max_length=500, null=True, blank= True)
    answer5=models.CharField(max_length=500, null=True, blank= True)

    requestcontent=models.CharField(max_length=100, null=True, blank= True)
    requestbutton=models.CharField(max_length=100, null=True, blank= True)


    content_panels = Page.content_panels + [
        FieldPanel("header_title"),
        FieldPanel("meta_content"),
        FieldPanel("subtitle"),
        FieldPanel("subtitle1"),
        StreamFieldPanel("content5"),
        FieldPanel("cardtitle1"),
        FieldPanel("cardtext1"),
        FieldPanel("cardtitle2"),
        FieldPanel("cardtext2"),
        FieldPanel("cardtitle3"),
        FieldPanel("cardtext3"),
        FieldPanel("cardtitle4"),
        FieldPanel("cardtext4"),
        FieldPanel("cardtitle5"),
        FieldPanel("cardtext5"),
        FieldPanel("cardtitle6"),
        FieldPanel("cardtext6"),
        FieldPanel("cardtitle7"),
        FieldPanel("cardtext7"),
        FieldPanel("cardtitle8"),
        FieldPanel("cardtext8"),
        
        StreamFieldPanel("content1"),
        StreamFieldPanel("content2"),
        StreamFieldPanel("content3"),
        StreamFieldPanel("content4"),
        FieldPanel("question_title"),
        FieldPanel("question1"),
        FieldPanel("answer1"),
        FieldPanel("question2"),
        FieldPanel("answer2"),
        FieldPanel("question3"),
        FieldPanel("answer3"),
        FieldPanel("question4"),
        FieldPanel("answer4"),
        FieldPanel("question5"),
        FieldPanel("answer5"),
        
        FieldPanel("requestcontent"),
        FieldPanel("requestbutton"),
    ]