from django.db import models
from wagtail.core.models import Page
from wagtail.admin.edit_handlers import PageChooserPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from streams import blocks
from wagtail.core.fields import StreamField

# Create your models here.

class IndustryPage(Page):

    template = "Industry/government.html"

    covid = models.ForeignKey('wagtailcore.Page', null=True,
        blank=False,
        on_delete=models.SET_NULL,
        related_name="+"),

    header_title = models.CharField(max_length=200, blank=True, null=True)
    meta_content = models.CharField(max_length=200, blank=True, null=True)    

    image = models.ForeignKey(
        "wagtailimages.Image",
        blank=False,
        null=True,
        related_name="+",
        on_delete=models.SET_NULL,
    )    

    subtitle = models.CharField(max_length=100, blank=True, null=True)

    content = StreamField(
        [
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
        ],
        null=True,
        blank=True,
    )
    
    requestcontent=models.CharField(max_length=100, null=True, blank= True)
    requestbutton=models.CharField(max_length=100, null=True, blank= True)

    content_panels = Page.content_panels + [
        FieldPanel("header_title"),
        FieldPanel("meta_content"),
        FieldPanel("subtitle"),
        ImageChooserPanel("image"),
        StreamFieldPanel("content"),
        FieldPanel("requestcontent"),
        FieldPanel("requestbutton"),
        
    ]    