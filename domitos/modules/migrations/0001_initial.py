# Generated by Django 3.2.7 on 2021-10-20 10:32

from django.db import migrations, models
import django.db.models.deletion
import streams.blocks
import wagtail.core.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('wagtailcore', '0062_comment_models_and_pagesubscription'),
        ('wagtailimages', '0023_add_choose_permissions'),
    ]

    operations = [
        migrations.CreateModel(
            name='ModulePage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.page')),
                ('header_title', models.CharField(blank=True, max_length=200, null=True)),
                ('meta_content', models.CharField(blank=True, max_length=200, null=True)),
                ('subtitle', models.CharField(blank=True, max_length=100, null=True)),
                ('content', wagtail.core.fields.StreamField([('full_richtext', streams.blocks.RichtextBlock())], blank=True, null=True)),
                ('requestcontent', models.CharField(blank=True, max_length=100, null=True)),
                ('requestbutton', models.CharField(blank=True, max_length=100, null=True)),
                ('image1', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailimages.image')),
                ('image2', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailimages.image')),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page',),
        ),
    ]
