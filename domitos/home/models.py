from django.db import models

# Create your models here.
from django.db import models
from wagtail.core.models import Page, Locale
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.core import blocks

from wagtail.core.fields import StreamField

from streams import blocks

from blog.models import BlogDetailPage



class HomePage(Page):
    
    templates = "home/home_page.html"
    
    def posts(self):
        posts = BlogDetailPage.objects.all()
        posts = posts.filter(locale=Locale.get_active()).order_by('-date')[:3]
        return posts
    
    header_title = models.CharField(max_length=200, blank=True, null=True)
    meta_content = models.CharField(max_length=200, blank=True, null=True)

    covid_title1=models.CharField(max_length=300, null=True, blank= True)
    covid_content1 = StreamField(
        [ 
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
        
        ],
        null=True,
        blank=True
    )
    covid_content2 = StreamField(
        [ 
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
        
        ],
        null=True,
        blank=True
    )

    facility_title1=models.CharField(max_length=300, null=True, blank= True)
    facility_content1 = StreamField(
        [ 
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
        
        ],
        null=True,
        blank=True
    )
    facility_content2 = StreamField(
        [ 
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
        
        ],
        null=True,
        blank=True
    )
    
    crm_title1=models.CharField(max_length=300, null=True, blank= True)
    crm_content1 = StreamField(
        [ 
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
        
        ],
        null=True,
        blank=True
    )
    crm_content2 = StreamField(
        [ 
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
        
        ],
        null=True,
        blank=True
    )

    assests_title1=models.CharField(max_length=300, null=True, blank= True)
    assests_content1 = StreamField(
        [ 
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
        
        ],
        null=True,
        blank=True
    )
    assests_content2 = StreamField(
        [ 
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
        
        ],
        null=True,
        blank=True
    )


    button1=models.CharField(max_length=100, null=True, blank= True)
    button2=models.CharField(max_length=100, null=True, blank= True)
    button3=models.CharField(max_length=100, null=True, blank= True)
    button4=models.CharField(max_length=100, null=True, blank= True)

    box_title1=models.CharField(max_length=300, null=True, blank= True)
    box_text1=models.CharField(max_length=300, null=True, blank= True)
    box_title2=models.CharField(max_length=300, null=True, blank= True)
    box_text2=models.CharField(max_length=300, null=True, blank= True)
    box_title3=models.CharField(max_length=300, null=True, blank= True)
    box_text3=models.CharField(max_length=300, null=True, blank= True)

    snippet_title=models.CharField(max_length=300, null=True, blank= True)

    snippet_title1=models.CharField(max_length=300, null=True, blank= True)
    snippet_content1= StreamField(
        [ 
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
         ],
        null=True,
        blank=True
    )   

    snippet_title2=models.CharField(max_length=300, null=True, blank= True)
    snippet_content2= StreamField(
        [ 
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
         ],
        null=True,
        blank=True
    )   
    snippet_title3=models.CharField(max_length=300, null=True, blank= True)
    snippet_content3= StreamField(
        [ 
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
         ],
        null=True,
        blank=True
    )   
    snippet_title4=models.CharField(max_length=300, null=True, blank= True)
    snippet_content4= StreamField(
        [ 
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
         ],
        null=True,
        blank=True
    )   
    snippet_title5=models.CharField(max_length=300, null=True, blank= True)
    snippet_content5= StreamField(
        [ 
           
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
         ],
        null=True,
        blank=True
    )   
    manage_covid_subtitle=models.CharField(max_length=300, null=True, blank= True)
    manage_covid_content= StreamField(
        [
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
         ],
        null=True,
        blank=True
    )   
    quote_content = StreamField(
        [ 
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
         ],
        null=True,
        blank=True
    )  
    
    plan1=models.CharField(max_length=300, null=True, blank= True)
    plan1_content = StreamField(
        [ 
            
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
         ],
        null=True,
        blank=True
    )   
    plan2=models.CharField(max_length=300, null=True, blank= True)
    plan2_point1=models.CharField(max_length=300, null=True, blank= True)
    plan2_point2=models.CharField(max_length=300, null=True, blank= True)
    plan2_point3=models.CharField(max_length=300, null=True, blank= True)
    plan2_point4=models.CharField(max_length=300, null=True, blank= True)
    plan2_point5=models.CharField(max_length=300, null=True, blank= True)
   
    plan3=models.CharField(max_length=300, null=True, blank= True)
    plan3_content = StreamField(
        [ 
            
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
         ],
        null=True,
        blank=True
    )    
    plan_button=models.CharField(max_length=300, null=True, blank= True)
    

   

    customer_content= StreamField(
        [ 
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
         ],
        null=True,
        blank=True
    )
    testimonials= StreamField(
        [ 
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
            ("cards", blocks.CardBlock()),
         ],
        null=True,
        blank=True
    )
   
    page_title_content = StreamField(
        [ 
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
         ],
        null=True,
        blank=True
    )
    total_project=models.CharField(max_length=100, null=True, blank= True)
    project_unit=models.CharField(max_length=100, null=True, blank= True)
    tenants_managed=models.CharField(max_length=100, null=True, blank= True)

    news_content= StreamField(
        [ 
            ("title_and_text", blocks.TitleAndTextBlock()),
            ("full_richtext", blocks.RichtextBlock()),
            ("simple_richtext", blocks.SimpleRichtextBlock()),
         ],
        null=True,
        blank=True
    )
    view_button=models.CharField(max_length=100, null=True, blank= True)

    requestcontent=models.CharField(max_length=100, null=True, blank= True)
    requestbutton=models.CharField(max_length=100, null=True, blank= True)
    content_panels = Page.content_panels + [
        FieldPanel("header_title"),
        FieldPanel("meta_content"),
        StreamFieldPanel("covid_title1"),
        StreamFieldPanel("covid_content1"),
        StreamFieldPanel("covid_content2"),
        StreamFieldPanel("facility_title1"),
        StreamFieldPanel("facility_content1"),
        StreamFieldPanel("facility_content2"),
        StreamFieldPanel("crm_title1"),
        StreamFieldPanel("crm_content1"),
        StreamFieldPanel("crm_content2"),
        StreamFieldPanel("assests_title1"),
        StreamFieldPanel("assests_content1"),
        StreamFieldPanel("assests_content2"),
        FieldPanel("button1"),
        FieldPanel("button2"),
        FieldPanel("button3"),
        FieldPanel("button4"),
        FieldPanel("box_title1"),
        FieldPanel("box_text1"),
        FieldPanel("box_title2"),
        FieldPanel("box_text2"),
        FieldPanel("box_title3"),
        FieldPanel("box_text3"),
        FieldPanel("snippet_title"),
        FieldPanel("snippet_title1"),
        StreamFieldPanel("snippet_content1"),
        FieldPanel("snippet_title2"),
        StreamFieldPanel("snippet_content2"),
        FieldPanel("snippet_title3"),
        StreamFieldPanel("snippet_content3"),
        FieldPanel("snippet_title4"),
        StreamFieldPanel("snippet_content4"),
        FieldPanel("snippet_title5"),
        StreamFieldPanel("snippet_content5"),
        FieldPanel("manage_covid_subtitle"),
        StreamFieldPanel("manage_covid_content"),
    
        StreamFieldPanel("quote_content"),
        FieldPanel("plan1"),
        StreamFieldPanel("plan1_content"),
        FieldPanel("plan2"),
        FieldPanel("plan2_point1"),
        FieldPanel("plan2_point2"),
        FieldPanel("plan2_point3"),
        FieldPanel("plan2_point4"),
        FieldPanel("plan2_point5"),
        FieldPanel("plan3"),
        StreamFieldPanel("plan3_content"),
        FieldPanel("plan_button"),

        StreamFieldPanel("customer_content"),
        StreamFieldPanel("testimonials"),
        StreamFieldPanel("page_title_content"),
        FieldPanel("total_project"),
        FieldPanel("project_unit"),
        FieldPanel("tenants_managed"),
        StreamFieldPanel("news_content"),
        FieldPanel("view_button"),
        FieldPanel("requestcontent"),
        FieldPanel("requestbutton"),

    ]
    