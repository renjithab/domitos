from django.db import models

from wagtail.core.models import Page
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.core import blocks

from wagtail.core.fields import StreamField

from streams import blocks



from modelcluster.fields import ParentalKey
from wagtail.admin.edit_handlers import (
    FieldPanel,
    FieldRowPanel,
    InlinePanel,
    MultiFieldPanel
)
from wagtail.core.fields import RichTextField
from wagtail.contrib.forms.models import (
    AbstractEmailForm,
    AbstractFormField
)
from wagtail.core.models import Page


class FormField(AbstractFormField):
    page = ParentalKey(
        'ContactPage',
        on_delete=models.CASCADE,
        related_name='form_fields',
    )


class ContactPage(AbstractEmailForm):

    template = "contact/contact_page.html"
    landing_page_template = "contact/contact_page_landing.html"
    
    header_title = models.CharField(max_length=200, blank=True, null=True)
    meta_content = models.CharField(max_length=200, blank=True, null=True) 

    content1 = StreamField(
        [ 
            ("full_richtext", blocks.RichtextBlock()),
        ],
        null=True,
        blank=True
    )
    content2 = StreamField(
        [ 
            ("full_richtext", blocks.RichtextBlock()),
        ],
        null=True,
        blank=True
    )
    emailID = StreamField(
        [ 
            ("full_richtext", blocks.RichtextBlock()),
        ],
        null=True,
        blank=True
    )
    phoneNo= StreamField(
        [ 
            ("full_richtext", blocks.RichtextBlock()),
        ],
        null=True,
        blank=True
    )

    time = StreamField(
        [ 
            ("full_richtext", blocks.RichtextBlock()),
        ],
        null=True,
        blank=True
    )

    requestcontent=models.CharField(max_length=100, null=True, blank= True)
    requestbutton=models.CharField(max_length=100, null=True, blank= True)
    
    
    button=models.CharField(max_length=100, null=True, blank= True)

    intro = RichTextField(blank=True)
    thank_you_text = RichTextField(blank=True)

    content_panels = AbstractEmailForm.content_panels + [
        FieldPanel('header_title'),
        FieldPanel('meta_content'),
        FieldPanel('intro'),
        InlinePanel('form_fields', label='Form Fields'),
        FieldPanel('thank_you_text'),
        MultiFieldPanel([
            FieldRowPanel([
                FieldPanel('from_address', classname="col6"),
                FieldPanel('to_address', classname="col6"),
            ]),
            FieldPanel("subject"),
        ], "Email Notification Config"),


        
        StreamFieldPanel('content1'),
        StreamFieldPanel('content2'),
        StreamFieldPanel('emailID'),
        StreamFieldPanel('phoneNo'),
        StreamFieldPanel('time'),
        FieldPanel('button'),
        FieldPanel("requestcontent"),
        FieldPanel("requestbutton"),
    ]

